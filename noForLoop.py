# inport the reduce from the funtools module

from functools import reduce

# define  a list of numbers

myList=[1,2,3,4,5]

# find the product

product=reduce((lambda x,y: x*y),myList)
print("the product of the numbers in the list is\n",product)


# or do this using the for loop

# initalise sum and product to 0 and 1 respectively

sum1=0
product1=1

for num in myList:
    sum1+=num

    product1*=num

print(f"the sum is {sum1} and the product is {product}")


mathOperations=('*','/','+','-')

for op in mathOperations:
    # print(op)
    
    if op=='*':
        result=1
        result*=num
    elif op=='+':
        result=sum(myList)
    elif op=='-':
        result=(myList[0]-(sum(myList[1:])))
    elif op=='/':
        result=myList[0]
        for num in myList[1:]:
            result/=num
    print(f"the answer for {op} operation is ",result)         



