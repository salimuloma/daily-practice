# python program to sort files according to date


import glob
import os

files= glob.glob("*.py")  #replace *.py with any file extension of your choice
files.sort(key=os.path.getmtime)

print("\n".join(files))